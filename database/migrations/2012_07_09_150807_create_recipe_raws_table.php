<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipeRawsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipe_raws', function (Blueprint $table) {
           $table->increments('id');
            $table->integer('recipe_id');
            $table->integer('product_id');
            $table->integer('unit_id')->nullable();
            $table->decimal('used_qty', 8, 3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipe_raws');
    }
}

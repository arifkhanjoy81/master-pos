@extends('layouts.app')
@section('title','Types')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Types
        <small>Manage Types</small>
    </h1>
    <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
    </ol> -->
</section>

<!-- Main content -->
<section class="content">

	<div class="box">
        <div class="box-header">
        	<h3 class="box-title">All Types</h3>
            @can('roles.create')
        	<div class="box-tools">
                <button type="button" class="btn btn-block btn-primary btn-modal" 
                    data-href="{{action('ProductTypeController@create')}}" 
                    data-container=".permission_modal">
                    <i class="fa fa-plus"></i> @lang( 'messages.add' )</button>
            </div>
            @endcan
        </div>
        <div class="box-body">
            <div class="table-responsive">
            @can('roles.view')
        	<table class="table table-bordered table-striped text-center" id="type_table">
        		<thead>
        			<tr>
        				<th>Types</th>
        				<th>@lang( 'messages.action' )</th>
        			</tr>
        		</thead>
        	</table>
            @endcan
            </div>
        </div>
    </div>

<div class="modal fade types_modal" tabindex="-1" role="dialog" 
        aria-labelledby="gridSystemModalLabel">
</div>

</section>
<!-- /.content -->
@stop
@section('javascript')
<script type="text/javascript">
    //Roles table
    $(document).ready( function(){
            var type_table = $('table#type_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ action('ProductTypeController@index') }}",
            columns: [
                {data: 'name',name:'name'},
                {data: 'action',name: 'action',orderable: false,searchable: false},
            ]
        });
        $(document).on('submit', 'form#raw_item_add_form', function(e){
        e.preventDefault();
        var data = $(this).serialize();
        
        $.ajax({
        method: "POST",
        url: $(this).attr("action"),
        dataType: "json",
        data: data,
        success: function(result){
            if(result.success === true){
                $('div.raw_item_modal').modal('hide');
                toastr.success(result.msg);
                type_table.ajax.reload();
            } else {
                toastr.error(result.msg);
            }
        }
        });
        });

        $(document).on('click', 'button.btn_delete', function(){
        swal({
        title: LANG.sure,
        text: "This Item will be delted!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        }).then((willDelete) => {
        if (willDelete) {
            var href = $(this).data('href');
            $.ajax({
                method: "DELETE",
                url: href,
                dataType: "json",
                data: data,
                success: function(result){
                    if(result.success === true){
                        toastr.success(result.msg);
                        location.reload();
                    } else {
                        toastr.error(result.msg);
                    }
                }
            });
        }
        });
        });
    });
</script>
@endsection

@extends('layouts.app')
@section('title','Permissions')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Permissions
        <small>Manage Permissions</small>
    </h1>
    <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
    </ol> -->
</section>

<!-- Main content -->
<section class="content">

	<div class="box">
        <div class="box-header">
        	<h3 class="box-title">All Permissions</h3>
            @can('roles.create')
        	<div class="box-tools">
                <button type="button" class="btn btn-block btn-primary btn-modal" 
                    data-href="{{action('PermissionController@create')}}" 
                    data-container=".permission_modal">
                    <i class="fa fa-plus"></i> @lang( 'messages.add' )</button>
            </div>
            @endcan
        </div>
        <div class="box-body">
            <div class="table-responsive">
            @can('roles.view')
        	<table class="table table-bordered table-striped" id="permissions_table">
        		<thead>
        			<tr>
        				<th>Permissions</th>
        				<th>@lang( 'messages.action' )</th>
        			</tr>
        		</thead>
        	</table>
            @endcan
            </div>
        </div>
    </div>

<div class="modal fade permission_modal" tabindex="-1" role="dialog" 
        aria-labelledby="gridSystemModalLabel">
</div>

</section>
<!-- /.content -->
@stop
@section('javascript')
<script type="text/javascript">
    //Roles table
    $(document).ready( function(){
        var roles_table = $('#permissions_table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: '/permissions',
                    buttons:[],
                    columnDefs: [ {
                        "targets": 1,
                        "orderable": false,
                        "searchable": false
                    } ]
                });
        $(document).on('click', 'button.delete_per_button', function(){
            swal({
              title: LANG.sure,
              text: LANG.confirm_delete_role,
              icon: "warning",
              buttons: true,
              dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    var href = $(this).data('href');
                    var data = $(this).serialize();

                    $.ajax({
                        method: "DELETE",
                        url: href,
                        dataType: "json",
                        data: data,
                        success: function(result){
                            if(result.success == true){
                                toastr.success(result.msg);
                                roles_table.ajax.reload();
                            } else {
                                toastr.error(result.msg);
                            }
                        }
                    });
                }
            });
        });
    });
</script>
@endsection

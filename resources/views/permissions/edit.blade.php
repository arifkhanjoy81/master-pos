<div class="modal-dialog" role="document">
  <div class="modal-content">

    <form method="post" action="{{ action('PermissionController@update',$per->id)}}">
      {{ method_field('PATCH') }}
      {{ csrf_field()}}
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">Add Permission</h4>
    </div>

    <div class="modal-body">
      <div class="form-group">
          <input type="text" name="name" value="{{$per->name}}" class="form-control">
      </div>

    <div class="modal-footer">
      <button type="submit" class="btn btn-primary">@lang( 'messages.save' )</button>
      <button type="button" class="btn btn-default" data-dismiss="modal">@lang( 'messages.close' )</button>
    </div>

    {!! Form::close() !!}

  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
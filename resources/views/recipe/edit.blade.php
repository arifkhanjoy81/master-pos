@extends('layouts.app')
@section('title', 'Update Recipe')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Update Recipe {{$recipe->product->name}}</h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="box">
      <div class="box-header">Add New Raw Item</div>
      <div class="box-body">
        <table class="table table-bordered table-striped text-cente">
            <thead>
              <tr>
                <th>Ready Product</th>
                <th>Location</th>
                <th>Action</th>
              </tr> 
            </thead>

            <tbody>
              <tr>
                <form method="post" action="{{ action('RecipeController@update',$recipe->id)}}">
                {{ csrf_field()}}
                {{ method_field('PATCH') }}
                <td>
                  <select class="form-control" name="product_id" required>
                      <option value="" hidden>Select A Product</option>
                      @foreach($products as $product)
                      <option value="{{$product->id}}" {{ ($recipe->product_id == $product->id) ?'selected':''}}>{{$product->name}}</option>
                      @endforeach
                  </select>
                </td>
                <td>
                  <select class="form-control" name="location_id" required>
                    @foreach($locations as $location)
                      <option value="{{$location->id}}" {{ ($recipe->location_id == $location->id) ?'selected':''}}>{{$location->name}}</option>
                      @endforeach
                  </select>
                </td>
                <td>
                  <button type="submit"class="btn btn-success bt-sm">Update</button>
                </td>
              </form>
              </tr> 
            </tbody>       
        </table>
        <hr>
        <h4>Update Recipe Raw Item</h4>
        <button type="button" data-href="{{action('RecipeController@newRawAdd',$recipe->id)}}" class="btn btn-primary bt-sm pull-right btn-modal" data-container=".recipe_modal">Add New Raw Item</button><br><br>
        <table class="table table-bordered table-striped text-center" id="recipe_table">
            <thead>
              <tr>
                <th>Raw product</th>
                <th>Unit</th>
                <th>Used Quantity</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($recipe->recipe_rows as $recipe_row)
              <tr>
                <td>{{$recipe_row->product->name}}</td>
                <td>{{$recipe_row->unit->actual_name}}</td>
                <td>{{$recipe_row->used_qty}}</td>
                <td>
                  <button type="button" data-href="{{action('RecipeController@rawEdit', [$recipe_row->id])}}" class="btn btn-xs btn-success btn-modal" data-container=".recipe_modal"><i class="glyphicon glyphicon-edit"></i>edit</button>

                  <button data-href="{{action('RecipeController@rawDelete', [$recipe_row->id])}}" class="btn btn-xs btn-danger btn_delete"><i class="glyphicon glyphicon-trash"></i>delete</button>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
      </div>

  </div>
<div class="modal fade recipe_modal" tabindex="-1" role="dialog" 
        aria-labelledby="gridSystemModalLabel">
</div>

</section>
<!-- /.content -->

@stop

@section('javascript')
<script type="text/javascript">
  var num=0;
  $(document).on('click','#append_button',function(){
    num ++;
    var row=`<div class="form-row" id="append`+num+`">
            <div class="form-group col-md-4">
              <label for="inputEmail4">Product</label>
              <select class="form-control" name="row_product_id[]" required>
                  <option value="" hidden>Select Your Product</option>
                  @foreach($products as $product)
                  <option value="{{$product->id}}">{{$product->name}}</option>
                  @endforeach
              </select>
            </div>
            <div class="form-group col-md-4">
              <label for="inputEmail4">Unit</label>
              <select class="form-control" name="row_unit_id[]" required>
                <option value="" hidden>Select Your Unit</option>
                @foreach($units as $unit)
                  <option value="{{$unit->id}}">{{$unit->actual_name}}</option>
                  @endforeach
              </select>
            </div>

            <div class="form-group col-md-3">
              <label for="inputEmail4">Quantity</label>
              <input type="number" name="row_used_qty[]" placeholder="Enter Quantity" class="form-control" required>
            </div>
            <div class="form-group col-md-1">
              <br><span style="color:#fff;border:1px solid #ccc; margin-top:10px;padding:4px; background-color:green;cursor:pointer;" id="append_button"><i class="glyphicon glyphicon-plus"></i></span>
                &nbsp;&nbsp;&nbsp;<span style="color:#fff;border:1px solid #ccc; margin-top:10px;padding:4px; background-color:red;cursor:pointer;" id="remove_button" data-id="`+num+`"><i class="glyphicon glyphicon-minus"></i></span>
            </div>
          </div>`;
    $('div#append_section').append(row);
  });
$(document).on('click','#remove_button',function(){
  var id=$(this).data('id'); 

  $('div#append'+id).remove();
});


$(document).on('click', 'button.btn_delete', function(){
    swal({
    title: LANG.sure,
    text: "This Item will be delted!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
    }).then((willDelete) => {
    if (willDelete) {
        var href = $(this).data('href');
        $.ajax({
            method: "get",
            url: href,
            success: function(result){
                if(result.success === true){
                    toastr.success(result.msg);
                    location.reload();
                } else {
                    toastr.error(result.msg);
                }
            }
        });
    }
    });
});
</script>
@endsection
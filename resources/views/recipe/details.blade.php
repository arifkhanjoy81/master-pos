<div class="modal-dialog modal-xl" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">Recipe Details -:{{$recipe->product->name}}</h4>
    </div>

    <div class="modal-body">
      <table class="table table-bordered table-striped text-center" id="recipe_table">
          <thead>
            <tr>
              <th>Raw product</th>
              <th>Unit</th>
              <th>Used Quantity</th>
            </tr>
          </thead>
          <tbody>
            @foreach($recipe->recipe_rows as $recipe_row)
            <tr>
              <td>{{$recipe_row->product->name}}</td>
              <td>{{$recipe_row->unit->actual_name}}</td>
              <td>{{$recipe_row->used_qty}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
    </div>

    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">@lang( 'messages.close' )</button>
    </div>

  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
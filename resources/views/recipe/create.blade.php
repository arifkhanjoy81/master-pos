@extends('layouts.app')
@section('title', 'Add New Recipe')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Add New Recipe</h1>
</section>

<!-- Main content -->
<section class="content">
	<div class="box">
        <div class="box-header">
		</div>

		<div class="box-body">
			<form action="{{ action('RecipeController@store')}}" method="post">
				{{ csrf_field()}}
			    <div class="form-row">
				    <div class="form-group col-md-6">
				    	<label for="inputEmail4">Product</label>
					    <select class="form-control" name="product_id" required>
					      	<option value="" hidden>Select A Product</option>
					      	@foreach($products as $product)
					      	<option value="{{$product->id}}">{{$product->name}}</option>
					      	@endforeach
					    </select>
				    </div>
				    <div class="form-group col-md-6">
				    	<label for="inputEmail4">Select Your Location</label>
					    <select class="form-control" name="location_id" required>
					    	@foreach($locations as $location)
					      	<option value="{{$location->id}}">{{$location->name}}</option>
					      	@endforeach
					    </select>
				    </div>
			    </div>
				<hr>
				<hr>
				<div id="append_section" class="col-md-12">
						<h4> Add Product Row Item</h4>
					<div class="form-row" id="append0">
					    <div class="form-group col-md-4">
					    	<label for="inputEmail4">Product</label>
						    <select class="form-control" name="row_product_id[]" required>
						      	<option value="" hidden>Select Your Product</option>
						      	@foreach($products as $product)
						      	<option value="{{$product->id}}">{{$product->name}}</option>
						      	@endforeach
						    </select>
					    </div>
					    <div class="form-group col-md-4">
					    	<label for="inputEmail4">Unit</label>
						    <select class="form-control" name="row_unit_id[]" required>
						    	<option value="" hidden>Select Your Unit</option>
						    	@foreach($units as $unit)
						      	<option value="{{$unit->id}}">{{$unit->actual_name}}</option>
						      	@endforeach
						    </select>
					    </div>

					    <div class="form-group col-md-3">
					    	<label for="inputEmail4">Quantity</label>
						    <input type="number" name="row_used_qty[]" placeholder="Enter Quantity" class="form-control" required>
					    </div>
					    <div class="form-group col-md-1">
					    	<br><span style="color:#fff;border:1px solid #ccc; margin-top:10px;padding:4px; background-color:green;cursor:pointer;" id="append_button"><i class="glyphicon glyphicon-plus"></i></span>
					    	&nbsp;&nbsp;&nbsp;<span style="color:#fff;border:1px solid #ccc; margin-top:10px;padding:4px; background-color:red;cursor:pointer;" id="remove_button" data-id="0"><i class="glyphicon glyphicon-minus"></i></span>

					    </div>
				    </div>
				</div>


				<div class="form-row">
					<div class="form-group col-md-12">
						<button type="submit" class="btn btn-primary">Save</button>
					</div>
				</div>
			</form>
		</div>

	</div>
</section>
<!-- /.content -->

@stop

@section('javascript')
<script type="text/javascript">
	var num=0;
	$(document).on('click','#append_button',function(){
		num ++;
		var row=`<div class="form-row" id="append`+num+`">
				    <div class="form-group col-md-4">
				    	<label for="inputEmail4">Product</label>
					    <select class="form-control" name="row_product_id[]" required>
					      	<option value="" hidden>Select Your Product</option>
					      	@foreach($products as $product)
					      	<option value="{{$product->id}}">{{$product->name}}</option>
					      	@endforeach
					    </select>
				    </div>
				    <div class="form-group col-md-4">
				    	<label for="inputEmail4">Unit</label>
					    <select class="form-control" name="row_unit_id[]" required>
					    	<option value="" hidden>Select Your Unit</option>
					    	@foreach($units as $unit)
					      	<option value="{{$unit->id}}">{{$unit->actual_name}}</option>
					      	@endforeach
					    </select>
				    </div>

				    <div class="form-group col-md-3">
				    	<label for="inputEmail4">Quantity</label>
					    <input type="number" name="row_used_qty[]" placeholder="Enter Quantity" class="form-control" required>
				    </div>
				    <div class="form-group col-md-1">
				    	<br><span style="color:#fff;border:1px solid #ccc; margin-top:10px;padding:4px; background-color:green;cursor:pointer;" id="append_button"><i class="glyphicon glyphicon-plus"></i></span>
					    	&nbsp;&nbsp;&nbsp;<span style="color:#fff;border:1px solid #ccc; margin-top:10px;padding:4px; background-color:red;cursor:pointer;" id="remove_button" data-id="`+num+`"><i class="glyphicon glyphicon-minus"></i></span>
				    </div>
			    </div>`;
		$('div#append_section').append(row);
	});
$(document).on('click','#remove_button',function(){
	var id=$(this).data('id'); 

	$('div#append'+id).remove();
})
</script>
@endsection
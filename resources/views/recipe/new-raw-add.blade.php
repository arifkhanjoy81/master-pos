<div class="modal-dialog modal-xl" role="document">
  <form method="post" action="{{action('RecipeController@storeNewRawAdd')}}">
    {{csrf_field()}}
    <input type="hidden" name="recipe_id" value="{{$id}}">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">New Raw Item Add </h4>
    </div>

    <div class="modal-body">
        <div id="append_section" class="col-md-12">
            <h4> Add Product Row Item</h4>
          <div class="form-row" id="append0">
              <div class="form-group col-md-4">
                <label for="inputEmail4">Product</label>
                <select class="form-control" name="row_product_id[]" required>
                    <option value="" hidden>Select Your Product</option>
                    @foreach($products as $product)
                    <option value="{{$product->id}}">{{$product->name}}</option>
                    @endforeach
                </select>
              </div>
              <div class="form-group col-md-4">
                <label for="inputEmail4">Unit</label>
                <select class="form-control" name="row_unit_id[]" required>
                  <option value="" hidden>Select Your Unit</option>
                  @foreach($units as $unit)
                    <option value="{{$unit->id}}">{{$unit->actual_name}}</option>
                    @endforeach
                </select>
              </div>

              <div class="form-group col-md-3">
                <label for="inputEmail4">Quantity</label>
                <input type="number" name="row_used_qty[]" placeholder="Enter Quantity" class="form-control" required>
              </div>
              <div class="form-group col-md-1">
                <br><span style="color:#fff;border:1px solid #ccc; margin-top:10px;padding:4px; background-color:green;cursor:pointer;" id="append_button"><i class="glyphicon glyphicon-plus"></i></span>
                &nbsp;&nbsp;&nbsp;<span style="color:#fff;border:1px solid #ccc; margin-top:10px;padding:4px; background-color:red;cursor:pointer;" id="remove_button" data-id="0"><i class="glyphicon glyphicon-minus"></i></span>

              </div>
            </div>
        </div>
    </div>

    <div class="modal-footer">
      <button type="submit" class="btn btn-primary">@lang( 'messages.save' )</button>
      <button type="button" class="btn btn-danger" data-dismiss="modal">@lang( 'messages.close' )</button>
    </div>

  </div><!-- /.modal-content -->
</form>
</div><!-- /.modal-dialog -->
<div class="modal-dialog modal-xl" role="document">
  <form method="post" action="{{ action('RecipeController@recipeRawUpdate')}}">
        {{ csrf_field()}}
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Recipe Raw Update</h4>
      </div>

      <div class="modal-body">
        <input type="hidden" name="id" value="{{$reciperaw->id}}">
          <div class="form-group col-md-4">
                <label for="inputEmail4">Product</label>
                <select class="form-control" name="product_id" required>
                    <option value="" hidden>Select Your Product</option>
                    @foreach($products as $product)
                    <option value="{{$product->id}}" {{($reciperaw->product_id==$product->id) ?'selected':''}}>{{$product->name}}</option>
                    @endforeach
                </select>
              </div>
              <div class="form-group col-md-4">
                <label for="inputEmail4">Unit</label>
                <select class="form-control" name="unit_id" required>
                  <option value="" hidden>Select Your Unit</option>
                  @foreach($units as $unit)
                    <option value="{{$unit->id}}" {{($reciperaw->unit_id==$unit->id) ?'selected':''}}>{{$unit->actual_name}}</option>
                    @endforeach
                </select>
              </div>

              <div class="form-group col-md-4">
                <label for="inputEmail4">Quantity</label>
                <input type="number" name="used_qty" value="{{$reciperaw->used_qty}}" class="form-control" required>
              </div>
      </div>

      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">@lang( 'messages.save' )</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">@lang( 'messages.close' )</button>
      </div>

    </div><!-- /.modal-content -->
  </form>
</div><!-- /.modal-dialog -->
@extends('layouts.app')
@section('title','Recipe')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Recipe
        <small>Manage Recipe</small>
    </h1>
    <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
    </ol> -->
</section>

<!-- Main content -->
<section class="content">

	<div class="box">
        <div class="box-header">
        	<h3 class="box-title">All Recipe</h3>
            @can('recipe.create')
        	<div class="box-tools">
                <a type="button" class="btn btn-block btn-primary" 
                    href="{{action('RecipeController@create')}}">
                    <i class="fa fa-plus"></i> @lang( 'messages.add' )</a>
            </div>
            @endcan
        </div>
        <div class="box-body">
            <div class="table-responsive">
            @can('recipe.view')
        	<table class="table table-bordered table-striped text-center" id="recipe_table">
        		<thead>
        			<tr>
                        <th>Product</th>
        				<th>Possible Quantity</th>
                        <th>Location</th>
                        <th>Action</th>
        			</tr>
        		</thead>
        	</table>
            @endcan
            </div>
        </div>
    </div>

<div class="modal fade recipe_modal" tabindex="-1" role="dialog" 
        aria-labelledby="gridSystemModalLabel">
</div>

</section>
<!-- /.content -->
@stop
@section('javascript')
<script type="text/javascript">
    //Roles table
   //Kitchen Raw Items table
   var recipe_table = $('table#recipe_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ action('RecipeController@index') }}",
        columns: [
            {data: 'product_name',name:'product_name'},
            {data: 'updated_at',name:'updated_at'},
            {data: 'location',name:'location'},
            {data: 'action',name: 'action',orderable: false,searchable: false},
        ]
    });
    $(document).on('submit', 'form#raw_item_add_form', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    
    $.ajax({
    method: "POST",
    url: $(this).attr("action"),
    dataType: "json",
    data: data,
    success: function(result){
        if(result.success === true){
            $('div.raw_item_modal').modal('hide');
            toastr.success(result.msg);
            recipe_table.ajax.reload();
        } else {
            toastr.error(result.msg);
        }
    }
    });
    });

    $(document).on('click', 'button.btn_delete', function(){
    swal({
    title: LANG.sure,
    text: "This Item will be delted!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
    }).then((willDelete) => {
    if (willDelete) {
        var href = $(this).data('href');
        $.ajax({
            method: "DELETE",
            url: href,
            dataType: "json",
            data: data,
            success: function(result){
                if(result.success === true){
                    toastr.success(result.msg);
                    location.reload();
                } else {
                    toastr.error(result.msg);
                }
            }
        });
    }
    });
    });
</script>
@endsection

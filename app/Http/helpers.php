<?php

if (! function_exists('humanFilesize')) {
    function humanFilesize($size, $precision = 2)
    {
        $units = ['B','kB','MB','GB','TB','PB','EB','ZB','YB'];
        $step = 1024;
        $i = 0;

        while (($size / $step) > 0.9) {
            $size = $size / $step;
            $i++;
        }
        
        return round($size, $precision).$units[$i];
    }
}


function getbusinessId(){
    return  auth()->user()->permitted_locations();
}

function businessId(){
    return request()->session()->get('user.business_id');
}

function getRecipeStock($recipe_rows){

    $qty=0;
    foreach ($recipe_rows as $key => $recipe_row) {
            $remain_stock=$recipe_row->product->variation_location_details->sum('qty_available');
        $unit_qty=0;
        if($recipe_row->unit->child_value){
            $unit_qty = $recipe_row->used_qty/$unit->child_value;
        }else{
            $unit_qty = $recipe_row->used_qty/1000;
        }

    $temp =$remain_stock/$unit_qty;
    $array[]=$temp;
    $value=min($array);
    $qty = $value;
    }
    return round($qty);
}


function decreaseRecipeStock($recipe_rows,$qty_difference){
    foreach($recipe_rows as $recipe_row){

            $stock =$recipe_row->product->variation_location_details->sum('qty_available');
            $qty = $recipe_row->used_quantity;
                $unit = $recipe_row->unit;

                if($unit->child_value){
                    $qty = $recipe_row->used_quantity/$unit->child_value;
                }else{
                    $qty = $recipe_row->used_quantity/1000;
                }

               $qty_update=$stock- ($qty*$qty_difference);
                $recipe_row->product->variation_location_details->update(['qty_available'=>$qty_update]);  
        }
}

<?php

namespace App\Http\Controllers;

use App\RecipeRaw;
use Illuminate\Http\Request;

class RecipeRawController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RecipeRaw  $recipeRaw
     * @return \Illuminate\Http\Response
     */
    public function show(RecipeRaw $recipeRaw)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RecipeRaw  $recipeRaw
     * @return \Illuminate\Http\Response
     */
    public function edit(RecipeRaw $recipeRaw)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RecipeRaw  $recipeRaw
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RecipeRaw $recipeRaw)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RecipeRaw  $recipeRaw
     * @return \Illuminate\Http\Response
     */
    public function destroy(RecipeRaw $recipeRaw)
    {
        //
    }
}

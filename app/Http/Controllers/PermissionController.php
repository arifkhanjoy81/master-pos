<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Permission;
use Yajra\DataTables\Facades\DataTables;
class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
    {
        if (!auth()->user()->can('roles.view')) {
            abort(403, 'Unauthorized action.');
        }

        if (request()->ajax()) {

            $data = Permission::orderby('id','desc')
                        ->select(['name', 'id']);
            return DataTables::of($data)
                ->addColumn('action', function ($row) {
    
                        $action = '';
                        if (auth()->user()->can('roles.update')) {
                            $action .= '<a data-href="' . action('PermissionController@edit', [$row->id]) . '" class="btn btn-xs btn-primary btn-modal" data-container=".permission_modal">
                            <i class="glyphicon glyphicon-edit"></i> ' . __("messages.edit") . '</a>';
                        }
                        if (auth()->user()->can('roles.delete')) {
                            $action .= '&nbsp
                                <button data-href="' . action('PermissionController@destroy', [$row->id]) . '" class="btn btn-xs btn-danger delete_per_button"><i class="glyphicon glyphicon-trash"></i> ' . __("messages.delete") . '</button>';
                        }
                        
                        return $action;
                })
                ->removeColumn('id')
                ->rawColumns([1])
                ->make(false);
        }

        return view('permissions.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('permissions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Permission::create(['name'=> $_POST['name'],'guard_name'=>'web']);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $per=Permission::find($id);
        return view('permissions.edit',compact('per'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Permission::where('id',$id)->update(['name'=> $_POST['name'],'guard_name'=>'web']);

        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

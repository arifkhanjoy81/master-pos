<?php

namespace App\Http\Controllers;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Request;
use App\Recipe;
use App\RecipeRaw;
use App\BusinessLocation;
use App\Product;
use App\Unit;
use DB;
class RecipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
        public function index()
    {

        if (request()->ajax()) {

            $query = Recipe::join('products','products.id','=','recipes.product_id')
                        ->join('business_locations','business_locations.id','=','recipes.location_id')
                        ->select('products.name as product_name','business_locations.name as location','recipes.created_at','recipes.updated_at','recipes.id')
                        ->with('recipe_rows','recipe_rows.product.variation_location_details','recipe_rows.unit')
                        ->where('recipes.business_id',businessId());
                        $permitted_locations = auth()->user()->permitted_locations();

                        if($permitted_locations != 'all') {
                            $query->whereIn('recipes.location_id',$permitted_locations);
                        }
                        $data=$query->latest()->get();
            return Datatables::of($data)
                ->editColumn('updated_at',function($row){
                    return getRecipeStock($row->recipe_rows);
                })
                ->addColumn('action', function($row){

                    $action = '';
                    if (auth()->user()->can('recipe.update')) {
                        $action .= '<a href="' . action('RecipeController@edit', [$row->id]) . '" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-edit"></i> ' . __("messages.edit") . '</a>';
                    }
                        $action .= '&nbsp
                            <button data-href="' . action('RecipeController@show', [$row->id]) . '" class="btn btn-xs btn-primary btn-modal" data-container=".recipe_modal"><i class="glyphicon glyphicon-ok-circle"></i> ' . 'details' . '</button>';
                    
                    return $action;

                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('recipe.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products=Product::select('name','id')->where('business_id',businessId())->get();
        $units=Unit::select('actual_name','id')->where('business_id',businessId())->get();
        $locations=BusinessLocation::where('business_id',businessId())
                    ->select('name','id')
                    ->get();
        return view('recipe.create',compact('products','locations','units'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $data=$r->only(['product_id','location_id']);
        $data['business_id']=request()->session()->get('user.business_id');
        $recipe=Recipe::create($data);

        if(isset($r->row_product_id)){
            foreach ($r->row_product_id as $key => $value) {
                RecipeRaw::create([
                    'recipe_id'=>$recipe->id,
                    'product_id'=>$value,
                    'unit_id'=>$r->row_unit_id[$key],
                    'used_qty'=>$r->row_used_qty[$key]
                ]);
            }
        }
        return redirect()->action('RecipeController@index')->with('s_message','Recipe Created !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $recipe=Recipe::with('location','product','recipe_rows','recipe_rows.product','recipe_rows.product.variation_location_details','recipe_rows.unit')
        ->find($id);
        return view('recipe.details',compact('recipe'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $products=Product::select('name','id')->where('business_id',businessId())->get();
        $units=Unit::select('actual_name','id')->where('business_id',businessId())->get();
        $locations=BusinessLocation::where('business_id',businessId())
                    ->select('name','id')
                    ->get();
        $recipe=Recipe::with('location','product','recipe_rows','recipe_rows.product','recipe_rows.product.variation_location_details','recipe_rows.unit')
                    ->find($id);
        return view('recipe.edit',compact('recipe','products','units','locations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        $data=$r->only(['product_id','location_id']);
        Recipe::where('id',$id)->update($data);
        return redirect()->action('RecipeController@index')->with('s_message','Recipe Updated !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function newRawAdd($id){
        $products=Product::select('name','id')->where('business_id',businessId())->get();
        $units=Unit::select('actual_name','id')->where('business_id',businessId())->get();
        return view('recipe.new-raw-add',compact('id','products','units'));
    }

    public function storeNewRawAdd (Request $r){
        foreach ($r->row_product_id as $key => $value) {
                RecipeRaw::create(['recipe_id'=>$r->recipe_id,'product_id'=>$value,'unit_id'=>$r->row_unit_id[$key],'used_qty'=>$r->row_used_qty[$key]]);
            }

            return back()->with('s_message','Recipe Raw Item Created !');
    }

    public function rawEdit($id){
        $products=Product::select('name','id')->where('business_id',businessId())->get();
        $units=Unit::select('actual_name','id')->where('business_id',businessId())->get();

        $reciperaw=RecipeRaw::find($id);
        return view('recipe.recipe_raw_edit',compact('products','units','reciperaw'));
    }

    public function recipeRawUpdate(Request $r){
        $data=$r->only(['product_id','unit_id','used_qty']);
         RecipeRaw::where('id',$_POST['id'])->update($data);
         return back()->with('s_message','Recipe Raw Item Updated !');
    }

    public function rawDelete($id){

        RecipeRaw::where('id',$id)->delete();
        return ['success' => true,
                'msg' => 'Recipe Raw Is deleted !'
                        ];
    }
}

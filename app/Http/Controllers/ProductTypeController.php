<?php

namespace App\Http\Controllers;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Request;
use App\ProductType;

class ProductTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {

            $data =ProductType::all(); 
            return Datatables::of($data)
                ->addColumn('action', function($row){

                    $action = '';
                    if (auth()->user()->can('roles.update')) {
                        $action .= '<a data-href="' . action('ProductTypeController@edit', [$row->id]) . '" class="btn btn-xs btn-success btn-modal" data-container=".types_modal"><i class="glyphicon glyphicon-edit"></i> ' . __("messages.edit") . '</a>';
                    }
                        $action .= '&nbsp
                            <button data-href="' . action('ProductTypeController@show', [$row->id]) . '" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> ' . 'delete' . '</button>';
                    
                    return $action;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('product_type.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         ProductType::create(['name'=> $_POST['name']]);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\productType  $productType
     * @return \Illuminate\Http\Response
     */
    public function show(productType $productType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\productType  $productType
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $type=ProductType::find($id);
        return view('product_type.edit',compact('type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\productType  $productType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         ProductType::where('id',$id)->update(['name'=> $_POST['name']]);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\productType  $productType
     * @return \Illuminate\Http\Response
     */
    public function destroy(productType $productType)
    {
        //
    }
}

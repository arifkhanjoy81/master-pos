<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    protected $guarded = ['id'];


    public function recipe_rows()
     {
       return $this->hasMany("App\RecipeRaw");
     }


     public function location()
     {
       return $this->belongsTo("App\BusinessLocation",'location_id');
     }

     public function product()
     {
       return $this->belongsTo("App\product",'product_id');
     }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipeRaw extends Model
{
    protected $guarded = ['id'];


    public function recipe()
     {
       return $this->belongsTo("App\Recipe");
     }

     public function product()
     {
       return $this->belongsTo("App\Product");
     }

     public function unit()
     {
       return $this->belongsTo("App\Unit");
     }
}
